package iss.pos.promotion;

import iss.pos.OrderItem;

public abstract class Promotion {
	public abstract double getOrderItemDiscountedPrice(OrderItem orderItem);
}
