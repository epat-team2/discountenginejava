package iss.pos.promotion;

import iss.pos.OrderItem;

public class Promotion1 extends Promotion{

	public Promotion1(){}
	
	public double getOrderItemDiscountedPrice(OrderItem orderItem){
		
		if (orderItem.getQuantity()== 1)
		return orderItem.getProduct().getPrice();
		
		else if ((orderItem.getQuantity())%2 == 0)
		return orderItem.getProduct().getPrice()+0.7* orderItem.getProduct().getPrice();
		
		return 0;
	}
	

}
