package iss.pos.promotion;

import iss.pos.OrderItem;

public class Promotion3 extends Promotion {

	public Promotion3(){
		
	}
	
	@Override
	public double getOrderItemDiscountedPrice(OrderItem orderItem) {
		// TODO Auto-generated method stub\
		if (orderItem.getProduct().getSku()=="2001.3")
		{
			int multiples = orderItem.getQuantity()/3;
			int remainder = orderItem.getQuantity()%3;
			double multiplePrice = 25 * multiples;
			double remainderPrice = remainder * 15;
			
			return multiplePrice + remainderPrice;
		}
		return orderItem.getProduct().getPrice()*orderItem.getQuantity();
	}

}
