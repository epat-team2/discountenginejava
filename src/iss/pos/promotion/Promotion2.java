package iss.pos.promotion;

import java.util.Iterator;
import java.util.List;

import iss.pos.OrderItem;

public class Promotion2 extends Promotion {

	public Promotion2 () {}
	
	@Override
	public double getOrderItemDiscountedPrice(OrderItem orderItem) {
			double finalprice = 0.0;
			
			if (orderItem.getQuantity() > 1)
				finalprice = 0.8;
			else
				finalprice = 0.9;
			
			return orderItem.getProduct().getPrice()*orderItem.getQuantity()*finalprice;

	}

}
