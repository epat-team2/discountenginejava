package iss.pos.test;
import iss.pos.Order;
import iss.pos.promotion.Promotion1;
import iss.pos.promotion.Promotion2;
import iss.pos.promotion.Promotion3;

import org.junit.Test;

public class SampleTest {
	
	private Order getOrder(){
        Order o = new Order();
        o.add(Products.GetProduct("redDress"), 3); 
        o.add(Products.GetProduct("greenDress"), 1); 
        o.add(Products.GetProduct("blueDress"), 3); 
        o.add(Products.GetProduct("White Socks"), 3); 
        o.add(Products.GetProduct("Invisible Socks"), 3); 
        return o;
	}
	
	@Test
	public void testPromo1() {

		Order order = getOrder();
		order.setPromo(new Promotion1());
		
		System.out.print("(Promotion 1) Total Price: " + order.getTotalPrice());
	}
	
	@Test
	public void testPromo2() {

		Order order = getOrder();
		order.setPromo(new Promotion2());
	
		System.out.print("(Promotion 2) Total Price: " + order.getTotalPrice());
	}
	
	@Test
	public void testPromo3() {

		Order order = getOrder();
		order.setPromo(new Promotion3());
	
		System.out.print("(Promotion 3) Total Price: " + order.getTotalPrice());
	}

}
