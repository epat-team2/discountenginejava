package iss.pos.test;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;

import iss.pos.Order;
import iss.pos.OrderItem;
import iss.pos.promotion.*;

import org.junit.Test;

public class Promotion2Test {
	
	private Order getOrder(){
        Order o = new Order();
        o.add(Products.GetProduct("blueDress"), 1); //90
        o.add(Products.GetProduct("redDress"), 2); //80 * 2
        o.add(Products.GetProduct("greenDress"), 3); // 80 *3 
        o.add(Products.GetProduct("whiteSocks"), 1); // 9
        o.add(Products.GetProduct("redSocks"), 2); // 8*2

        return o;
	}
	
	@Test
	public void testTotalOrder() {
		Order myOrder = new Order();
		myOrder.add(Products.GetProduct("blueDress"), 1); //90
		myOrder.add(Products.GetProduct("redDress"), 2); //80 * 2
		myOrder.add(Products.GetProduct("greenDress"), 3); // 80 *3 
		myOrder.add(Products.GetProduct("whiteSocks"), 1); // 9
		myOrder.add(Products.GetProduct("redSocks"), 2); // 8*2

		Promotion2 promo2 = new Promotion2();
		double totalPrice = 0.0;
		
		totalPrice += promo2.getOrderItemDiscountedPrice(myOrder.getOrderItem("1001.1"));
		totalPrice += promo2.getOrderItemDiscountedPrice(myOrder.getOrderItem("1001.2"));		
		totalPrice += promo2.getOrderItemDiscountedPrice(myOrder.getOrderItem("1001.3"));
		totalPrice += promo2.getOrderItemDiscountedPrice(myOrder.getOrderItem("2001.1"));
		totalPrice += promo2.getOrderItemDiscountedPrice(myOrder.getOrderItem("2001.2"));
//		assertEquals(515.0, myOrder.getTotalPrice(), 0.001);
		assertEquals(515.0, totalPrice, 0.001);
		
	}


}
