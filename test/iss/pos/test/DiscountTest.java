package iss.pos.test;

import static org.junit.Assert.*;

import org.junit.Test;

import iss.pos.*;
import iss.pos.promotion.*;

public class DiscountTest {
/*
	@Test
	public void test1ABuy1Item() {
		
		//setup
//		Promotion promo = new Promotion(); //TODO: setup the promotion as you see fit
//		DiscountCalculator dc = new DiscountCalculator(promo);
		
//		Order order = null; //TODO: setup the order, you can refer to SampleTest.java for example
		
		//exercise
	//	Order newOrder = dc.calculateDiscount(order);
		
		//verify
//		double expectedValue = 0;//TODO: set the expected value;
//assertEquals(expectedValue, newOrder.getTotalPrice(),0.001);
        //TODO: add additional verification if necessary
	}*/

	
	@Test
	public void testDiscount1ABuy1Item(){

		//Setup
		Promotion promo = new Promotion1(); //TODO: setup the promotion as you see fit
	
		Order order = new Order();
		order.add(Products.GetProduct("blueDress"), 1);

		 
		//exerice
		double price = promo.getOrderItemDiscountedPrice(order.getOrderItem("1001.3"));	
		
		//verify
		
		double expectedValue = 100;
		System.out.println(expectedValue + " " + price);
		assertEquals(expectedValue, price,0.001);

		
	}
	
	@Test
	public void testDiscount1BBuy2Items (){
		//Setup
		Promotion promo = new Promotion1(); //TODO: setup the promotion as you see fit

		Order order = new Order();
		order.add(Products.GetProduct("blueDress"), 2);

		 
		//exerice
		
		double price = promo.getOrderItemDiscountedPrice(order.getOrderItem("1001.3"));	
		//verify
		double expectedValue = 170;
		System.out.println(expectedValue + " " + price);
		assertEquals(expectedValue, price,0.001);
	}
	
	//@Test
	/*
	public void testDiscount1DBuyOddNumberItems (){
		//Setup
		Promotion promo = new Promotion1(); //TODO: setup the promotion as you see fit

		Order order = new Order();
		order.add(Products.GetProduct("blueDress"), 3);

		 
		//exerice
		
		double price = promo.getOrderItemDiscountedPrice(order.getOrderItem("1001.3"));	
		//verify
		double expectedValue = 100+70+100;
		System.out.println(expectedValue + " " + price);
		assertEquals(expectedValue, price,0.001);
	}
	*/

	@Test
	public void testType3Discount(){
		Promotion3 promo = new Promotion3();
		Order order = new Order(); //TODO: setup the order, you can refer to SampleTest.java for example
		order.add(Products.GetProduct("invisibleSocks"), 1);
		double onePrice = promo.getOrderItemDiscountedPrice(order.getOrderItem("2001.3"));
		assertEquals(15,onePrice,0.001);
		order.add(Products.GetProduct("invisibleSocks"), 2);
		double threePrice = promo.getOrderItemDiscountedPrice(order.getOrderItem("2001.3"));
		assertEquals(25,threePrice,0.001);		
		order.add(Products.GetProduct("invisibleSocks"), 2);
		double fivePrice = promo.getOrderItemDiscountedPrice(order.getOrderItem("2001.3"));
		assertEquals(55,fivePrice,0.001);		
	}

}

